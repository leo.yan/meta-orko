# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRCREV = "569283251545476c43177e5bc91d2541ac993e9d"

XEN_BRANCH = "v4.18-rc2-xen-aosp"

SRC_URI = " \
    git://gitlab.com/Linaro/blueprints/automotive/xen-aosp/xen.git;branch=${XEN_BRANCH};protocol=https \
    file://0001-menuconfig-mconf-cfg-Allow-specification-of-ncurses-location.patch \
    file://0001-Fixup-smmu-v3.patch \
    file://ioreq.cfg"
