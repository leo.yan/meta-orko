# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRCREV = "569283251545476c43177e5bc91d2541ac993e9d"

XEN_BRANCH = "v4.18-rc2-xen-aosp"

SRC_URI = " \
        git://gitlab.com/Linaro/blueprints/automotive/xen-aosp/xen.git;branch=${XEN_BRANCH};protocol=https \
        file://0001-Rename-to-xenphv.patch \
        file://0001-Enable-sdl-and-gl-test.patch \
        "

# Qemu is necessary on ARM platforms, and to support HVM guests on x86
QEMU_HVM_DEFAULT = "qemu ${@bb.utils.contains('DISTRO_FEATURES', 'vmsep', 'qemu-system-i386', '', d)}"
QEMU = "${@bb.utils.contains('PACKAGECONFIG', 'hvm', '${QEMU_HVM_DEFAULT}', '', d)}"
QEMU:arm = "qemu ${@bb.utils.contains('DISTRO_FEATURES', 'vmsep', 'qemu-system-i386', '', d)}"
QEMU:aarch64 = "qemu ${@bb.utils.contains('DISTRO_FEATURES', 'vmsep', 'qemu-system-aarch64', '', d)}"

QEMU_ARCH = "i386"
QEMU_ARCH:aarch64 = "aarch64"

EXTRA_OECONF:remove = " --with-system-qemu=${bindir}/qemu-system-i386"
EXTRA_OECONF:append = " --with-system-qemu=${bindir}/qemu-system-${QEMU_ARCH}"
