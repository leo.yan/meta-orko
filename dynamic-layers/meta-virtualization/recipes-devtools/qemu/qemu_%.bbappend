SRCREV = "54d02ce18a7d32278c8fb78dee31f08469b47b0d"

SRC_URI = " \
    gitsm://gitlab.com/Linaro/blueprints/automotive/xen-aosp/qemu.git;branch=upstream_maintenance_20230718;protocol=https \
    file://run-ptest \
    file://0003-apic-fixup-fallthrough-to-PIC.patch \
    file://0005-qemu-Do-not-include-file-if-not-exists.patch \
    file://0006-qemu-Add-some-user-space-mmap-tweaks-to-address-musl.patch \
    file://0007-qemu-Determinism-fixes.patch \
    file://0008-tests-meson.build-use-relative-path-to-refer-to-file.patch \
    file://0009-Define-MAP_SYNC-and-MAP_SHARED_VALIDATE-on-needed-li.patch \
    file://0010-hw-pvrdma-Protect-against-buggy-or-malicious-guest-d.patch \
    file://0002-linux-user-Replace-use-of-lfs64-related-functions-an.patch \
    file://qemu-guest-agent.init \
    file://qemu-guest-agent.udev \
"

EXTRA_OECONF:remove = "--disable-download"

PV = "git${SRCPV}"

S = "${WORKDIR}/git"

# Install qemu-system-aarch64
PACKAGES:prepend:class-target:append = " ${PN}-system-aarch64"
FILES:${PN}-aarch64:class-target = "${bindir}/qemu-aarch64"
FILES:${PN}-system-aarch64:class-target = "${bindir}/qemu-system-aarch64"
RDEPENDS:${PN}-system-aarch64:append:class-target = "${PN}"
INSANE_SKIP:${PN}-system-aarch64:class-target = "file-rdeps"
